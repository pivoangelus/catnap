//
//  HookNode.swift
//  CatNap
//
//  Created by Justin Andros on 12/15/15.
//  Copyright © 2015 Justin Andros. All rights reserved.
//

import SpriteKit

class HookNode: SKSpriteNode, CustomNodeEvents {
    
    private var hookNode = SKSpriteNode(imageNamed: "hook")
    private var ropeNode = SKSpriteNode(imageNamed: "rope")
    private var hookJoint: SKPhysicsJointFixed!
    
    var isHooked: Bool {
        return hookJoint != nil
    }
    
    func didMoveToScene() {
        // following lines are equivalent to unchecking dynamic for the node, or setting physicsBody = false
        guard let scene = scene else { return }
        let ceilingFix = SKPhysicsJointFixed.jointWithBodyA(scene.physicsBody!, bodyB: physicsBody!, anchor: CGPoint.zero)
        scene.physicsWorld.addJoint(ceilingFix)
        
        // add rope
        ropeNode.anchorPoint = CGPoint(x: 0, y: 0.5)
        ropeNode.zRotation = CGFloat(270).degreesToRadians()
        ropeNode.position = position
        scene.addChild(ropeNode)
        
        // add hook
        hookNode.position = CGPoint(x: position.x, y: position.y - ropeNode.size.width)
        hookNode.physicsBody = SKPhysicsBody(circleOfRadius: hookNode.size.width / 2)
        hookNode.physicsBody!.categoryBitMask = PhysicsCategory.Hook
        hookNode.physicsBody!.contactTestBitMask = PhysicsCategory.Cat
        hookNode.physicsBody!.collisionBitMask = PhysicsCategory.None
        scene.addChild(hookNode)
        // and attach it to the rope
        let hookPosition = CGPoint(x: hookNode.position.x, y: hookNode.position.y + hookNode.size.height / 2)
        let ropeJoint = SKPhysicsJointSpring.jointWithBodyA(physicsBody!, bodyB: hookNode.physicsBody!, anchorA: position, anchorB: hookPosition)
        scene.physicsWorld.addJoint(ropeJoint)
        
        // rope always "faces" the hook appearing attached
        let range = SKRange(lowerLimit: 0.0, upperLimit: 0.0)
        let orientConstraint = SKConstraint.orientToNode(hookNode, offset: range)
        ropeNode.constraints = [orientConstraint]
        
        // hook moves left to right for a little bit
        hookNode.physicsBody!.applyImpulse(CGVectorMake(50, 0))
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "catTapped", name: kCatTappedNotification, object: nil)
    }
    
    func catTapped() {
        if isHooked {
            releaseCat()
        }
    }
    
    func hookCat(catNode: SKNode) {
        print("cat hooked")
        catNode.parent!.physicsBody!.velocity = CGVectorMake(0, 0)
        catNode.parent!.physicsBody!.angularVelocity = 0
        
        let pinPoint = CGPoint(x: hookNode.position.x, y: hookNode.position.y + hookNode.size.height / 2)
        hookJoint = SKPhysicsJointFixed.jointWithBodyA(hookNode.physicsBody!, bodyB: catNode.parent!.physicsBody!, anchor: pinPoint)
        scene!.physicsWorld.addJoint(hookJoint)
        hookNode.physicsBody!.contactTestBitMask = PhysicsCategory.None
    }
    
    func releaseCat() {
        hookNode.physicsBody!.categoryBitMask = PhysicsCategory.None
        hookNode.physicsBody!.contactTestBitMask = PhysicsCategory.None
        hookJoint.bodyA.node!.zRotation = 0
        hookJoint.bodyB.node!.zRotation = 0
        scene!.physicsWorld.removeJoint(hookJoint)
        hookJoint = nil
    }
}
