//
//  DiscoBallNode.swift
//  CatNap
//
//  Created by Justin Andros on 12/16/15.
//  Copyright © 2015 Justin Andros. All rights reserved.
//

import SpriteKit
import AVFoundation

class DiscoBallNode: SKSpriteNode, CustomNodeEvents, InteractiveNode {
    
    private var player: AVPlayer!
    private var video: SKVideoNode!
    private var isDiscoTime: Bool = false {
        didSet {
            video.hidden = !isDiscoTime
            if isDiscoTime {
                video.play()
                runAction(spinAction)
            } else {
                video.pause()
                removeAllActions()
            }
            SKTAudio.sharedInstance().playBackgroundMusic(
                isDiscoTime ? "media/disco-sound.m4a" : "backgroundMusic.mp3"
            )
            if isDiscoTime {
                video.runAction(SKAction.waitForDuration(5.0), completion: {
                    self.isDiscoTime = false
                })
            }
            DiscoBallNode.isDiscoTime = isDiscoTime
        }
    }
    static private(set) var isDiscoTime = false
    private let spinAction = SKAction.repeatActionForever(
        SKAction.animateWithTextures([
            SKTexture(imageNamed: "discoball1"),
            SKTexture(imageNamed: "discoball2"),
            SKTexture(imageNamed: "discoball3")
            ], timePerFrame: 0.2)
    )
    
    func didMoveToScene() {
        userInteractionEnabled = true
        let fileURL = NSBundle.mainBundle().URLForResource("media/discolights-loop", withExtension: "mov")!
        player = AVPlayer(URL: fileURL)
        video = SKVideoNode(AVPlayer: player)
        video.size = scene!.size
        video.position = CGPoint(x: CGRectGetMidX(scene!.frame),
                                 y: CGRectGetMidY(scene!.frame))
        video.zPosition = -1
        scene!.addChild(video)
        video.alpha = 0.75
        video.hidden = true
        video.pause()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "didReachEndOfVideo", name: AVPlayerItemDidPlayToEndTimeNotification, object: nil)
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesEnded(touches, withEvent: event)
        interact()
    }
    
    func interact() {
        if !isDiscoTime {
            isDiscoTime = true
        }
    }

    func didReachEndOfVideo() {
        print("rewind!")
        player.currentItem!.seekToTime(kCMTimeZero)
    }
    
}
