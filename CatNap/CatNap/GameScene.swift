//
//  GameScene.swift
//  CatNap
//
//  Created by Justin Andros on 11/25/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

import SpriteKit

protocol CustomNodeEvents {
    func didMoveToScene()
}

protocol InteractiveNode {
    func interact()
}

struct PhysicsCategory {
    static let None:    UInt32 = 0x0        // 0
    static let Cat:     UInt32 = 0x1        // 1
    static let Block:   UInt32 = 0x1 << 1   // 2
    static let Bed:     UInt32 = 0x1 << 2   // 4
    static let Edge:    UInt32 = 0x1 << 3   // 8
    static let Label:   UInt32 = 0x1 << 4   // 16
    static let Spring:  UInt32 = 0x1 << 5   // 32
    static let Hook:    UInt32 = 0x1 << 6   // 64
}

class GameScene: SKScene, SKPhysicsContactDelegate {
    var bedNode: BedNode!
    var catNode: CatNode!
    var hookNode: HookNode?
    var playable = true
    var currentLevel: Int = 0
    
    class func level(levelNum: Int) -> GameScene? {
        let scene = GameScene(fileNamed: "Level\(levelNum)")!
        scene.currentLevel = levelNum
        scene.scaleMode = .AspectFill
        return scene
    }
    
    override func didMoveToView(view: SKView) {
        SKTAudio.sharedInstance().playBackgroundMusic("backgroundMusic.mp3")
        
        let maxAspectRatio: CGFloat = 16.0/9.0 // iPhone 5.0
        let maxAspectRatioHeight = size.width / maxAspectRatio
        let playableMargin: CGFloat = (size.height - maxAspectRatioHeight) / 2
        let playableRect = CGRect(x: 0, y: playableMargin, width: size.width, height: size.height - playableMargin * 2)
        physicsBody = SKPhysicsBody(edgeLoopFromRect: playableRect)
        physicsWorld.contactDelegate = self
        physicsBody!.categoryBitMask = PhysicsCategory.Edge
        
        enumerateChildNodesWithName("//*", usingBlock:  {node, _ in
            if let customNode = node as? CustomNodeEvents {
                customNode.didMoveToScene()
            }
        })
        
        bedNode = childNodeWithName("bed") as! BedNode
        catNode = childNodeWithName("//cat_body") as! CatNode
        
        // limits the cat's rotation
        //let rotationConstraint = SKConstraint.zRotation(SKRange(lowerLimit: CGFloat(-M_PI / 4), upperLimit: CGFloat(M_PI / Double(4))))
        //catNode.parent!.constraints = [rotationConstraint]
        
        hookNode = childNodeWithName("hookBase") as? HookNode
    }
    
    override func didSimulatePhysics() {
        if playable && hookNode?.isHooked != true {
            if fabs(catNode.parent!.zRotation) > CGFloat(25).degreesToRadians() {
                lose()
            }
        }
    }
    
    func didBeginContact(contact: SKPhysicsContact) {
        if !playable { return }
        let collision = contact.bodyA.categoryBitMask | contact.bodyB.categoryBitMask
        
        if collision == PhysicsCategory.Cat | PhysicsCategory.Hook && hookNode?.isHooked == false {
            print("here")
            hookNode!.hookCat(catNode)
        }
        
        if collision == PhysicsCategory.Cat | PhysicsCategory.Bed {
            print("success")
            win()
        } else if collision == PhysicsCategory.Cat | PhysicsCategory.Edge {
            print("fail")
            lose()
        }
    }
    
    func inGameMessage(text: String) {
        let message = MessageNode(message: text)
        message.position = CGPoint(
                                x: CGRectGetMidX(frame),
                                y: CGRectGetMidY(frame)
                            )
        addChild(message)
    }
    
    func newGame() {
        view!.presentScene(GameScene.level(currentLevel))
    }
    
    func lose() {
        if (currentLevel > 1) { currentLevel-- }
        playable = false
        SKTAudio.sharedInstance().pauseBackgroundMusic()
        runAction(SKAction.playSoundFileNamed("lose.mp3", waitForCompletion: false))
        inGameMessage("Try again")
        performSelector("newGame", withObject: nil, afterDelay: 5)
        catNode.wakeUp()
    }
    
    func win() {
        if (currentLevel < 6) { currentLevel++ }
        playable = false
        SKTAudio.sharedInstance().pauseBackgroundMusic()
        runAction(SKAction.playSoundFileNamed("win.mp3", waitForCompletion: false))
        inGameMessage("Nice job!")
        performSelector("newGame", withObject: nil, afterDelay: 3)
        catNode.curlAt(bedNode.position)
    }
}
