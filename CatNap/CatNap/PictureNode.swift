//
//  PictureNode.swift
//  CatNap
//
//  Created by Justin Andros on 12/16/15.
//  Copyright © 2015 Justin Andros. All rights reserved.
//

import SpriteKit

class PictureNode: SKSpriteNode, CustomNodeEvents, InteractiveNode {
    
    func didMoveToScene() {
        userInteractionEnabled = true
        let pictureNode = SKSpriteNode(imageNamed: "picture")
        let maskNode = SKSpriteNode(imageNamed: "picture-frame-mask")
        let cropNode = SKCropNode()
        cropNode.addChild(pictureNode)
        cropNode.maskNode = maskNode
        addChild(cropNode)
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesEnded(touches, withEvent: event)
        interact()
    }
    
    func interact() {
        userInteractionEnabled = false
        physicsBody!.dynamic = true
    }

}
