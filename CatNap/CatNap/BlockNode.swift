//
//  BlockNode.swift
//  CatNap
//
//  Created by Justin Andros on 11/25/15.
//  Copyright © 2015 Justin Andros. All rights reserved.
//

import SpriteKit

class BlockNode: SKSpriteNode, CustomNodeEvents, InteractiveNode {
    
    func didMoveToScene() {
        physicsBody!.categoryBitMask = PhysicsCategory.Block
        physicsBody!.collisionBitMask = PhysicsCategory.Cat | PhysicsCategory.Block | PhysicsCategory.Edge | PhysicsCategory.Spring
        userInteractionEnabled = true
    }
    
    func interact() {
        userInteractionEnabled = false
        
        runAction(
            SKAction.sequence([
                SKAction.playSoundFileNamed("pop.mp3", waitForCompletion: false),
                SKAction.scaleTo(0.8, duration: 0.1),
                SKAction.removeFromParent()
                ])
        )
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesEnded(touches, withEvent: event)
        print("destory block")
        interact()
    }
    
}
